<?php
	
return 
	array(
		'AUTH_CHECK_URL' =>  'http://auth-server.l/check',
		'AUTH_LOGOUT_URL' =>  'http://auth-server.l/logout',
		'COOKIE_EXPIRE' =>  time()+3600,
		'AUTH_TOKEN_NAME' =>  'auth_server_token'
	);
	

?>